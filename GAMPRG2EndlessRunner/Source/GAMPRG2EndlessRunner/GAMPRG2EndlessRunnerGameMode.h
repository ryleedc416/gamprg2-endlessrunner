// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "GAMPRG2EndlessRunnerGameMode.generated.h"

UCLASS(minimalapi)
class AGAMPRG2EndlessRunnerGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AGAMPRG2EndlessRunnerGameMode();
};



