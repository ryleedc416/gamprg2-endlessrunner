// Fill out your copyright notice in the Description page of Project Settings.


#include "RunCharacterController.h"
#include "RunCharacter.h"

void ARunCharacterController::BeginPlay()
{
	Super::BeginPlay();

	//Actor = Cast<ARunCharacter>(GetPawn());
	Actor = GetPawn<ARunCharacter>();
}

void ARunCharacterController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ARunCharacterController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAxis("MoveRight", this, &ARunCharacterController::MoveRight);
	InputComponent->BindAxis("MoveForward", this, &ARunCharacterController::MoveForward);
}

void ARunCharacterController::MoveRight(float axis)
{
	Actor->AddMovementInput(Actor->GetActorRightVector(), axis);
}

void ARunCharacterController::MoveForward(float axis)
{
	Actor->AddMovementInput(Actor->GetActorForwardVector(), 1.0f);
}

void ARunCharacterController::Disable()
{
	DisableInput(Cast<ARunCharacterController>(this));
}
