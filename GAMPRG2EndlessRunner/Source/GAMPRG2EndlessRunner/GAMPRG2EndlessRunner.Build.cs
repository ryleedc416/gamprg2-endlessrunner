// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class GAMPRG2EndlessRunner : ModuleRules
{
	public GAMPRG2EndlessRunner(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay" });
	}
}
