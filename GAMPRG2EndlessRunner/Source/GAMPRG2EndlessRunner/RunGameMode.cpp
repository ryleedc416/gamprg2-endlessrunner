// Fill out your copyright notice in the Description page of Project Settings.


#include "RunGameMode.h"
#include "Engine/World.h"
#include "Tile.h"
#include "Obstacle.h"
#include "TimerManager.h"

void ARunGameMode::BeginPlay()
{
	Super::BeginPlay();

	for (size_t i = 0; i < 5; i++)
	{
		AddFloorTile();
	}
}

void ARunGameMode::OnExited(ATile* tiles)
{
	AddFloorTile();
	GetWorldTimerManager().SetTimer(timerHandle, FTimerDelegate::CreateUObject(this, &ARunGameMode::DestroyTile, tiles), 1.0f, true);
}

void ARunGameMode::AddFloorTile()
{
	FActorSpawnParameters spawnParams;
	ATile* tile = GetWorld()->SpawnActor<ATile>(TileClass, next, spawnParams);
	next = tile->GetAttachTransform();
	tile->OnExited.AddDynamic(this, &ARunGameMode::OnExited);
	UE_LOG(LogTemp, Warning, TEXT("Tile created: %s %s"), *tile->GetName(), *next.GetLocation().ToString());
}

void ARunGameMode::DestroyTile(ATile* tile)
{
	tile->Destroy();
}
