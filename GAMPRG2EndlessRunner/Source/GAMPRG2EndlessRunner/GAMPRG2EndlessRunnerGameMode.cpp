// Copyright Epic Games, Inc. All Rights Reserved.

#include "GAMPRG2EndlessRunnerGameMode.h"
#include "GAMPRG2EndlessRunnerCharacter.h"
#include "UObject/ConstructorHelpers.h"

AGAMPRG2EndlessRunnerGameMode::AGAMPRG2EndlessRunnerGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/ThirdPersonCPP/Blueprints/ThirdPersonCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
