// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "RunCharacterController.generated.h"

/**
 * 
 */
UCLASS()
class GAMPRG2ENDLESSRUNNER_API ARunCharacterController : public APlayerController
{
	GENERATED_BODY()
	
protected:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	virtual void SetupInputComponent() override;

	void MoveRight(float axis);
	void MoveForward(float axis);

	UPROPERTY(BlueprintReadOnly)
	class ARunCharacter* Actor;

public:
	UFUNCTION()
		void Disable();
};
