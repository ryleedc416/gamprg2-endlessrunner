// Fill out your copyright notice in the Description page of Project Settings.


#include "Tile.h"
#include "Components/SceneComponent.h"
#include "Components/ArrowComponent.h"
#include "Components/BoxComponent.h"
#include "RunCharacter.h"
#include "Obstacle.h"
#include "Pickup.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
ATile::ATile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Scene = CreateDefaultSubobject<USceneComponent>("Scene");
	AttachPoint = CreateDefaultSubobject<UArrowComponent>("AttachPoint");
	Box = CreateDefaultSubobject<UBoxComponent>("Box");
	SpawnBox1 = CreateDefaultSubobject<UBoxComponent>("SpawnBox1");
	SpawnBox2 = CreateDefaultSubobject<UBoxComponent>("SpawnBox2");

	SetRootComponent(Scene);
	AttachPoint->SetupAttachment(Scene);
	Box->SetupAttachment(Scene);
	SpawnBox1->SetupAttachment(Scene);
	SpawnBox2->SetupAttachment(Scene);
	
}

// Called when the game starts or when spawned
void ATile::BeginPlay()
{
	Super::BeginPlay();
	Box->OnComponentBeginOverlap.AddDynamic(this, &ATile::OnExit);
	WhichToSpawn();
}

FTransform ATile::GetAttachTransform()
{
	FTransform attach;
	attach = AttachPoint->GetComponentTransform();
	return attach;
}

void ATile::OnExit(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	UE_LOG(LogTemp, Warning, TEXT("AAAAAAAA"));
	if (ARunCharacter* character = Cast<ARunCharacter>(OtherActor))
	{
		OnExited.Broadcast(this);
	}
}

void ATile::SpawnObstacle()
{
	FActorSpawnParameters spawnInfo;
	AObstacle* obstacle = GetWorld()->SpawnActor<AObstacle>(ObstacleTypes[0], GetBox(SpawnBox1), spawnInfo);
	obstacle->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);
	SpawnedObjects.Add(obstacle);
}

void ATile::SpawnPickup()
{
	FActorSpawnParameters spawnInfo;
	APickup* pickup = GetWorld()->SpawnActor<APickup>(PickupTypes[0], GetBox(SpawnBox2), spawnInfo);
	pickup->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);
	SpawnedObjects.Add(pickup);
}

// Called every frame
void ATile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATile::WhichToSpawn()
{
	int32 rand = FMath::RandRange(0, 50);

	if (rand <= 25)
	{
		SpawnObstacle();
	}
	else if(rand > 25)
	{
		for (size_t i = 0; i < 3; i++)
		{
			SpawnPickup();
		}
	}
}

void ATile::Destroyed()
{
	for (AActor* spawnedObject : SpawnedObjects)
	{
		spawnedObject->Destroy();
	}
}

FVector ATile::GetRandomPointInBoundingBox(UBoxComponent* ChooseBox)
{
	FVector Origin = ChooseBox->Bounds.Origin;
	FVector Extent = ChooseBox->Bounds.BoxExtent;
	return UKismetMathLibrary::RandomPointInBoundingBox(Origin, Extent);
}

FTransform ATile::GetBox(UBoxComponent* WhichBox)
{
	FVector location = GetRandomPointInBoundingBox(WhichBox);
	FRotator rotation = GetRandomPointInBoundingBox(WhichBox).Rotation();
	FVector scale = FVector(1.0, 1.0, 1.0);
	return UKismetMathLibrary::MakeTransform(location, rotation, scale);
}

