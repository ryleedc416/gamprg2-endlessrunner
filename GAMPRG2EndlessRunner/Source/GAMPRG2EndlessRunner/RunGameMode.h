// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "RunGameMode.generated.h"

/**
 * 
 */
UCLASS()
class GAMPRG2ENDLESSRUNNER_API ARunGameMode : public AGameModeBase
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

	UFUNCTION()
		void OnExited(class ATile* tile);

	UPROPERTY(EditAnywhere, Category = "TileSpawn")
	class TSubclassOf<class ATile>TileClass;

	UFUNCTION()
		void AddFloorTile();

	UFUNCTION()
		void DestroyTile(ATile* tile);

private:
	FTransform next;

	FTimerHandle timerHandle;
};
