// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Tile.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FExitedSignature, class ATile*, Tile);

UCLASS()
class GAMPRG2ENDLESSRUNNER_API ATile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATile();

	FTransform GetAttachTransform();

	UPROPERTY(BlueprintAssignable)
		FExitedSignature OnExited;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class USceneComponent* Scene;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UArrowComponent* AttachPoint;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UBoxComponent* Box;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UBoxComponent* SpawnBox1;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		class UBoxComponent* SpawnBox2;

	UFUNCTION()
		void OnExit(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	
	UFUNCTION()
		void SpawnObstacle();

	UFUNCTION()
		void SpawnPickup();

	UFUNCTION()
		void WhichToSpawn();

	virtual void Destroyed() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	FVector GetRandomPointInBoundingBox(UBoxComponent* ChooseBox);
	FTransform GetBox(UBoxComponent* WhichBox);

	UPROPERTY(EditAnywhere, Category = "ObstacleSpawnable")
		TArray<TSubclassOf<class AObstacle>> ObstacleTypes;

	UPROPERTY(EditAnywhere, Category = "ObstacleSpawnable")
		TArray<TSubclassOf<class APickup>> PickupTypes;

private:
	TArray<class AActor*> SpawnedObjects;

};
